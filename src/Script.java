/************************************************
*
* @author Nolan O'Shea
* Assignment: Assignment 5
* Class: CS 4352
*
************************************************/

import java.io.*;
import java.util.*;

public class Script {
	private static final String FILE_IN = "assignment5_input.txt",
								FILE_OUT = "assignment5_output.txt";
	private static final double DENSITY = .65;
	
	public static void main(String[] args) {
		// master graph
		var g = new Graph();
		
		// load file
		try(var in = new Scanner(new BufferedInputStream(new FileInputStream(new File(FILE_IN))))) {
			// for each line
			while(in.hasNext()) {
				String v = in.next(),
					   w = in.next();
				
				// add both vertices to master graph if new
				if(!g.getVertices().contains(v)) {
					g.addV(v);
				} if(!g.getVertices().contains(w)) {
					g.addV(w);
				}
				
				// add each vertex as each others neighbor
				g.addNeighbors(v, w);
				
				// add Edge
				g.addEdge(Graph.getSet(v, w));
			}
		} catch(Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
		
		// Call cut algorithm to get clusters
		// Convert to sortable List
		var clusters = new ArrayList<Graph>(cut(g, new HashSet<Graph>()));
		
		// sort clusters by size
		Collections.sort(clusters, (c1,  c2) -> Integer.compare(c2.getVertices().size(), c1.getVertices().size()));
		
		try(var out = new PrintStream(new BufferedOutputStream(new FileOutputStream(new File(FILE_OUT))))) {
			// print each cluster
			for(Graph cluster : clusters) {
				out.println(cluster);
			}
		} catch(Exception e) {
	        System.err.println(e.getMessage());
	        e.printStackTrace();
	    }
	}

	/**
	 * Run clustering cut algorithm
	 * 
	 * @param g Graph to cut
	 * @param clusters Set to add clusters to
	 * 
	 * @return clusters
	 */
	private static Set<Graph> cut(Graph g, Set<Graph> clusters) {
		// Run betweenness algorithm
		g.betweenness();
		
		// Eliminate the edge of the greatest betweenness iteratively until the graph is separated.
		while(g.isConnected()) {
			g.cut();
		}
		
		for(Graph subG : g.getSubGraphs()) {
			// If its size is smaller than or equal to 2, delete the sub-graph.
			if(subG.getVertices().size() <= 2) {
				continue; // skip
			}
			
			// If its size is greater than 2 and its density is higher than or equal to a density threshold,
			// then return the sub-graph as a cluster.
			if(subG.getDensity() >= DENSITY) {
				clusters.add(subG);
			} else {
				// If its size is greater than 2 and its density is lower than a density threshold,
				// then call Step-1 and 2 with the sub-graph recursively.
				cut(subG, clusters);
			}
		}
		return clusters;
	}
}
