/************************************************
*
* @author Nolan O'Shea
* Assignment: Assignment 5
* Class: CS 4352
*
************************************************/

import java.util.*;

public class Graph {
	// Key is vertex, Value is NeighborSet
	private Map<String, Set<String>> vertices;
	private List<Set<String>> edges;
	
	public Graph() {
		vertices = new HashMap<String, Set<String>>();
		edges = new ArrayList<Set<String>>();
	}
	
	public Graph(Set<String> v) {
		this();
		// copy vertices and init neighborSets
		for(String w : v) {
			this.vertices.put(w, new HashSet<String>());
		}
	}
	
	// Sorts List of Edges by betweenness
	public void betweenness() {
		// init edge values
		var betweenness = new HashMap<Set<String>, Double>();
		for(Set<String> e : edges) {
			betweenness.put(e, 0.0); // init to zero
		}
		
		// for every vertex
		for(String s : vertices.keySet()) {
			var stack = new ArrayDeque<String>(); // empty stack
			
			var stats = new HashMap<String, Struct>();
			for(String v : vertices.keySet()) {
				stats.put(v, new Struct());
			}
			
			stats.get(s).setD(0).setSigma(1); // init start
			
			var q = new LinkedList<String>(); // empty queue
			q.add(s);
			
			// BFS
			while(q.size() > 0) { // while not empty
				String v = q.poll(); // dequeue
				stack.push(v);
				
				Struct vs = stats.get(v);
				
				// for each neighbor
				for(String w : vertices.get(v)) {
					Struct ws = stats.get(w);
					
					// if first time seeing w
					if(ws.d < 0) {
						q.add(w);
						ws.setD(vs.d + 1);
					}
					
					// if shortest path
					if(ws.d == vs.d + 1) {
						ws.sigma += vs.sigma;
						ws.p.add(v); // predecessor
					}
				}
			}
			// init deltas
			var deltas = new HashMap<String, Double>();
			for(String v : vertices.keySet()) {
				deltas.put(v, 0.0); // init to zero
			}
			
			// S returns vertices in order of non-increasing distance from s
			while(!stack.isEmpty()) {
				String w = stack.pop();
				
				Struct ws = stats.get(w);
				
				// for each predecessor
				for(String v : ws.p) {
					// create edge
					Set<String> e = getSet(v, w);
					
					// factor to update betweenness by
					double update = (1 + deltas.get(w))*stats.get(v).sigma/ws.sigma,
						   d = betweenness.get(e);
					betweenness.put(e, d + update);
					d = deltas.get(v);
					deltas.put(v, d + update);
				}
			}
		}
		// sort edges by betweenness
		Collections.sort(edges, (e1,  e2) -> Double.compare(betweenness.get(e2), betweenness.get(e1)));
	}
	
	/**
	 * Returns subGraphs
	 * 
	 * @return disjoint subGraphs
	 */
	public Set<Graph> getSubGraphs() {
		// first subGraph
		var g1 = new Graph(getConnected());
		
		var subG = new HashSet<String>(vertices.keySet());
		// get isolated vertices
		subG.removeAll(g1.getVertices());
		
		// second subGraph
		var g2 = new Graph(subG);
		
		// distribute all Edges to its corresponding subGraph
		for(Set<String> e : edges) {
			Iterator<String> iterator = e.iterator();
			String v = iterator.next(),
				   w = iterator.next();
			
			// add Edge and Neighbors to corresponding subGraph
			if(g1.getVertices().containsAll(e)) {
				g1.addEdge(e).addNeighbors(v, w);
			} else {
				g2.addEdge(e).addNeighbors(v, w);
			}
		}	
		// package up the subGraphs
		return getSet(g1, g2);
	}
	
	private Set<String> getConnected() {
		// arbitrary start
		String s = vertices.keySet().iterator().next();
		
		var connected = new HashSet<String>();
		var stack = new ArrayDeque<String>(); // empty stack
		stack.push(s);
		
		// DFS
		while(!stack.isEmpty()) { // while not empty
			String v = stack.pop();
			connected.add(v);
			
			// for each neighbor
			for(String w : vertices.get(v)) {
				if(!connected.contains(w)) {
					stack.push(w);
				}
			}
		}	
		return connected;
	}
	
	// Cuts first edge, assumes that List is sorted
	public void cut() {
		Set<String> cut = edges.remove(0);
		
		Iterator<String> iterator = cut.iterator();
		String v = iterator.next(),
			   w = iterator.next();
		
		// remove neighbors
		vertices.get(v).remove(w);
		vertices.get(w).remove(v);
	}
	
	/**
	 * Computes whether this Graph is disconnected
	 * 
	 * @return whether this Graph is disconnected
	 */
	public boolean isConnected() {
		return getConnected().containsAll(vertices.keySet());
	}
	
	/**
	 * Returns density
	 * 
	 * @return density
	 */
	public double getDensity() {
		int v = vertices.keySet().size(); // number of vertices
		return 2.0*edges.size()/(v*v - v);
	}
	
	/**
	 * Adds two vertices as neighbors
	 * 
	 * @param v first vertex
	 * @param w second vertex
	 */
	public Graph addNeighbors(String v, String w) {
		vertices.get(v).add(w);
		vertices.get(w).add(v);
		return this; // fluid interface
	}

	// Generic factory method
	public static <T> Set<T> getSet(T e1, T e2) {
		var v = new HashSet<T>();
		v.add(e1);
		v.add(e2);
		return v;
	}
	
	public Set<String> getVertices() {
		return new HashSet<String>(vertices.keySet()); // encapsulation
	}
	
	public void addV(String v) {
		vertices.put(v, new HashSet<String>());
	}
	
	public Graph addEdge(Set<String> e) {
		// no duplicates
		if(!edges.contains(e)) {
			edges.add(e);
		}
		return this; // fluid interface
	}

	@Override
	public String toString() {
		return vertices.size() + " : " + vertices.keySet();
	}
}

// Struct to contain stats for betweenness algorithm
class Struct {
	public int d, sigma;
	public Set<String> p;
	
	public Struct() {
		p = new HashSet<String>(); // empty list
		// init values
		d = -1;
		sigma = 0;
	}

	@Override
	public String toString() {
		return "Struct [d=" + d + ", sigma=" + sigma + ", p=" + p + "]";
	}

	public Struct setD(int d) {
		this.d = d;
		return this; // fluid interface
	}

	public Struct setSigma(int sigma) {
		this.sigma = sigma;
		return this; // fluid interface
	}
}
